package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;
	PolylineOptions po;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		//setRequestedOrientation(ApplicationInfo.)
		po = new PolylineOptions();
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		map.setMyLocationEnabled(true); 
		
		
		
		
		
		
		//map.setMyLocationEnabled(true);
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		
		MarkerOptions n0 = new MarkerOptions();
		n0.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
		n0.title("Hello");
		//map.addMarker(n0);
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				
				
				
				po.width(2);
				
				po.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				po.add(new LatLng(l.getLatitude(),l.getLongitude()));
				
				map.addPolyline(po);
				currentLocation = l;
				
				
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, locListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
				case R.id.action_mark:
					MarkerOptions n0 = new MarkerOptions();
					n0.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
					n0.title("Hello");
					map.addMarker(n0);
					break;
				case R.id.action_randcolor:
					Random r = new Random();
					
					po.color(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
					break;
					/*
					po = new PolylineOptions();
					
					po.width(2);
					
					po.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
					po.add(new LatLng(currentLocation.getLatitude()+0.01,currentLocation.getLongitude()+0.01));
					
					map.addPolyline(po);
					*/
		}
		return super.onOptionsItemSelected(item);
	}
	

	

}
